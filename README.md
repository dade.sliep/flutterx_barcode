# flutterx_barcode

Barcode flutter plugin for reading/writing barcode in multiple formats

## Import

Import the library like this:

```dart
import 'package:flutterx_barcode/flutterx_barcode.dart';
```

## Usage

Check the documentation in the desired source file of this library