library flutterx_barcode;

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutterx_barcode/src/model/barcode_format.dart';

export 'package:flutterx_barcode/src/barcode_image.dart';
export 'package:flutterx_barcode/src/barcode_view.dart';
export 'package:flutterx_barcode/src/barcode_view_raw.dart';
export 'package:flutterx_barcode/src/model/barcode.dart';
export 'package:flutterx_barcode/src/model/barcode_format.dart';
export 'package:flutterx_barcode/src/model/barcode_status.dart';
export 'package:flutterx_barcode/src/model/camera_facing.dart';

class FlutterxBarcode {
  static const MethodChannel _channel = MethodChannel('flutterx_barcode');

  static Future<Uint8List> encodeBitmap(String contents, BarcodeFormat format, int width, int height) async =>
      await _channel.invokeMethod('encodeBitmap', {
        'contents': contents,
        'format': describeEnum(format).toUpperCase(),
        'width': width,
        'height': height,
      });
}
