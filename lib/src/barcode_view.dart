import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_barcode/src/barcode_view_raw.dart';
import 'package:flutterx_barcode/src/model/barcode.dart';
import 'package:flutterx_barcode/src/model/barcode_format.dart';
import 'package:flutterx_barcode/src/model/camera_facing.dart';

class BarcodeView extends StatefulWidget {
  final List<BarcodeFormat> formats;
  final CameraFacing cameraFacing;
  final bool cameraEnabled;
  final bool flashEnabled;
  final bool scanEnabled;
  final void Function(Barcode barcode) onBarcode;

  const BarcodeView({
    Key? key,
    this.formats = const [],
    this.cameraFacing = CameraFacing.back,
    this.cameraEnabled = true,
    this.flashEnabled = false,
    this.scanEnabled = true,
    required this.onBarcode,
  }) : super(key: key);

  @override
  State<BarcodeView> createState() => _BarcodeViewState();
}

class _BarcodeViewState extends State<BarcodeView> {
  BarcodeViewController? _controller;

  @override
  void didUpdateWidget(covariant BarcodeView oldWidget) {
    super.didUpdateWidget(oldWidget);
    final controller = _controller;
    if (controller == null) return;
    if (widget.cameraFacing != oldWidget.cameraFacing) controller.setCameraFacing(widget.cameraFacing);
    if (widget.cameraEnabled != oldWidget.cameraEnabled) controller.setCameraEnabled(widget.cameraEnabled);
    if (widget.flashEnabled != oldWidget.flashEnabled) controller.enableFlash(widget.flashEnabled);
    if (widget.scanEnabled != oldWidget.scanEnabled) {
      widget.scanEnabled ? controller.startScan(widget.formats) : controller.stopScan();
    } else if (widget.scanEnabled && !listEquals(oldWidget.formats, widget.formats)) {
      controller.stopScan();
      controller.startScan(widget.formats);
    }
  }

  @override
  Widget build(BuildContext context) => RawBarcodeView(onBarcodeViewCreated: (controller) async {
        final status = await controller.statusInfo();
        if (!status.hasCameraPermission && !await controller.requirePermission()) return;
        controller.dataStream.listen((event) => widget.onBarcode(event));
        if (widget.cameraFacing != status.cameraFacing) controller.setCameraFacing(widget.cameraFacing);
        if (widget.cameraEnabled != status.cameraEnabled) controller.setCameraEnabled(widget.cameraEnabled);
        if (widget.flashEnabled != status.flashEnabled) controller.enableFlash(widget.flashEnabled);
        if (widget.scanEnabled != status.scanEnabled) {
          widget.scanEnabled ? controller.startScan(widget.formats) : controller.stopScan();
        }
        _controller = controller;
      });
}
