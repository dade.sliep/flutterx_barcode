import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutterx_barcode/flutterx_barcode.dart';
import 'package:flutterx_barcode/src/model/barcode.dart';
import 'package:flutterx_barcode/src/model/barcode_format.dart';

class BarcodeImage extends StatelessWidget {
  final String contents;
  final BarcodeFormat format;
  final int width;
  final int height;
  final Widget placeholder;

  const BarcodeImage({
    Key? key,
    required this.contents,
    required this.format,
    required this.width,
    required this.height,
    this.placeholder = const SizedBox.shrink(),
  }) : super(key: key);

  BarcodeImage.fromBarcode({
    Key? key,
    required Barcode barcode,
    required this.width,
    required this.height,
    this.placeholder = const SizedBox.shrink(),
  })  : contents = barcode.text,
        format = barcode.barcodeFormat,
        super(key: key);

  @override
  Widget build(BuildContext context) => FutureBuilder<Uint8List>(
      future: FlutterxBarcode.encodeBitmap(contents, format, width, height),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Image.memory(snapshot.requireData, width: width.toDouble(), height: height.toDouble());
        } else if (snapshot.hasError) {
          throw snapshot.error!;
        } else {
          return placeholder;
        }
      });
}
