import 'package:flutterx_barcode/src/model/camera_facing.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

/// Current status of [RawBarcodeView]
class BarcodeStatus with DTO {
  /// Camera permission allowed to application
  final bool hasCameraPermission;

  /// Available hardware camera devices
  final List<CameraFacing> availableCameras;

  /// Flash hardware feature available
  final bool hasFlash;

  /// Current camera facing
  final CameraFacing cameraFacing;

  /// Camera currently enabled
  final bool cameraEnabled;

  /// Flash currently enabled
  final bool flashEnabled;

  /// Barcode scan currently running
  final bool scanEnabled;

  const BarcodeStatus({
    required this.hasCameraPermission,
    required this.availableCameras,
    required this.hasFlash,
    required this.cameraFacing,
    required this.cameraEnabled,
    required this.flashEnabled,
    required this.scanEnabled,
  });

  static BarcodeStatus fromJson(JsonObject json) => BarcodeStatus(
        hasCameraPermission: json['hasCameraPermission'],
        availableCameras:
            (json['availableCameras'] as List).map((i) => CameraFacing.values[i as int]).toList(growable: false),
        hasFlash: json['hasFlash'],
        cameraFacing: (json['cameraFacing'] as int).let((i) => CameraFacing.values[i == -1 ? 0 : i]),
        cameraEnabled: json['cameraEnabled'],
        flashEnabled: json['flashEnabled'],
        scanEnabled: json['scanEnabled'],
      );

  @override
  JsonObject toJson() => {
        'hasCameraPermission': hasCameraPermission,
        'availableCameras': availableCameras.map((e) => e.index).toList(growable: false),
        'hasFlash': hasFlash,
        'cameraFacing': cameraFacing.index,
        'cameraEnabled': cameraEnabled,
        'flashEnabled': flashEnabled,
        'scanEnabled': scanEnabled,
      };
}
