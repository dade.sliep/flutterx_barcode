/// Enumerates barcode formats known to this package. Please keep alphabetized.
enum BarcodeFormat {
  /// Aztec 2D barcode format.
  aztec,

  /// CODABAR 1D format.
  codbar,

  /// Code 39 1D format.
  code_39,

  /// Code 93 1D format.
  code_93,

  /// Code 128 1D format.
  code_128,

  /// Data Matrix 2D barcode format.
  data_matrix,

  /// EAN-8 1D format.
  ean_8,

  /// EAN-13 1D format.
  ean_13,

  /// ITF (Interleaved Two of Five) 1D format.
  itf,

  /// MaxiCode 2D barcode format.
  maxicode,

  /// PDF417 format.
  pdf_417,

  /// QR Code 2D barcode format.
  qr_code,

  /// RSS 14
  rss_14,

  /// RSS EXPANDED
  rss_expanded,

  /// UPC-A 1D format.
  upc_a,

  /// UPC-E 1D format.
  upc_e,

  /// UPC/EAN extension format. Not a stand-alone format.
  upc_ean_extension,
}
