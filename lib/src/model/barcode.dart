import 'package:flutter/foundation.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

import 'barcode_format.dart';

/// Barcode element
class Barcode with DTO {
  /// Decoded text (String representation of [rawBytes])
  final String text;

  /// Decoded raw bytes of barcode
  final List<int> rawBytes;

  /// Barcode format
  final BarcodeFormat barcodeFormat;

  const Barcode({
    required this.text,
    required this.rawBytes,
    required this.barcodeFormat,
  });

  static Barcode fromJson(JsonObject json) => Barcode(
        text: json['text'],
        rawBytes: json['rawBytes'],
        barcodeFormat: BarcodeFormat.values.fromName(json['barcodeFormat'].toString().toLowerCase())!,
      );

  @override
  JsonObject toJson() => {
        'text': text,
        'rawBytes': rawBytes,
        'barcodeFormat': describeEnum(barcodeFormat).toUpperCase(),
      };
}
