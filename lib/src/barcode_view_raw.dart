import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterx_barcode/src/model/barcode.dart';
import 'package:flutterx_barcode/src/model/barcode_format.dart';
import 'package:flutterx_barcode/src/model/barcode_status.dart';
import 'package:flutterx_barcode/src/model/camera_facing.dart';

part 'barcode_view_controller.dart';

/// Raw barcode view that interacts with native plugin
/// It exposes a [BarcodeViewController] for interacting with barcode/camera API
class RawBarcodeView extends StatefulWidget {
  static const String _viewType = "flutterx_barcode_view";

  final void Function(BarcodeViewController controller) onBarcodeViewCreated;

  const RawBarcodeView({Key? key, required this.onBarcodeViewCreated}) : super(key: key);

  @override
  State<RawBarcodeView> createState() => _RawBarcodeViewState();
}

class _RawBarcodeViewState extends State<RawBarcodeView> {
  late BarcodeViewController _controller;

  @override
  Widget build(BuildContext context) {
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return AndroidView(viewType: RawBarcodeView._viewType, onPlatformViewCreated: _onPlatformViewCreated);
      case TargetPlatform.iOS:
        return UiKitView(viewType: RawBarcodeView._viewType, onPlatformViewCreated: _onPlatformViewCreated);
      default:
        throw UnsupportedError('BarcodeView is not supported on platform: $defaultTargetPlatform');
    }
  }

  void _onPlatformViewCreated(int id) {
    _controller = BarcodeViewController._(MethodChannel('${RawBarcodeView._viewType}/$id'));
    widget.onBarcodeViewCreated(_controller);
  }

  @override
  void dispose() {
    _controller._dispose();
    super.dispose();
  }
}
