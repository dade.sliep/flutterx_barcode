part of 'barcode_view_raw.dart';

class BarcodeViewController {
  final MethodChannel _channel;
  final StreamController<Barcode> _scanUpdateController = StreamController<Barcode>();

  Stream<Barcode> get dataStream => _scanUpdateController.stream;

  BarcodeViewController._(this._channel) {
    _channel.setMethodCallHandler((call) async {
      switch (call.method) {
        case 'onBarcodeResult':
          final barcode = Barcode.fromJson((call.arguments as Map).cast());
          _scanUpdateController.sink.add(barcode);
          break;
      }
    });
  }

  Future<void> startScan([List<BarcodeFormat> barcodeFormats = const []]) => _channel.invokeMethod(
      'scanStart', barcodeFormats.map((e) => describeEnum(e).toUpperCase()).toList(growable: false));

  Future<void> stopScan() => _channel.invokeMethod('scanStop');

  Future<bool> setCameraEnabled(bool enabled) async => await _channel.invokeMethod('setCameraEnabled', enabled);

  Future<bool> setCameraFacing(CameraFacing facing) async =>
      await _channel.invokeMethod('setCameraFacing', facing.index);

  Future<bool> enableFlash(bool enabled) async => await _channel.invokeMethod('enableFlash', enabled);

  Future<BarcodeStatus> statusInfo() async =>
      BarcodeStatus.fromJson((await _channel.invokeMethod('statusInfo') as Map).cast());

  Future<bool> requirePermission() async => await _channel.invokeMethod('requirePermission');

  void _dispose() => _scanUpdateController.close();
}
