package sliep.flutterx.flutterx_barcode

import android.Manifest.permission.CAMERA
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager.*
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeView
import io.flutter.plugin.common.*
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory
import sliep.flutterx.flutterx_barcode.FlutterxBarcodePlugin.Companion.activityBinding
import sliep.flutterx.flutterx_barcode.FlutterxBarcodePlugin.Companion.runMethod

class BarcodeView(
    private val context: Context,
    private val id: Int,
    private val channel: MethodChannel
) : PlatformView,
    MethodChannel.MethodCallHandler, PluginRegistry.RequestPermissionsResultListener {
    private var flashEnabled: Boolean = false
    private var cameraEnabled: Boolean = false
    private var scanEnabled: Boolean = false
    private lateinit var barcodeView: BarcodeView

    private val hasCameraPermission: Boolean
        get() = checkSelfPermission(context, CAMERA) == PERMISSION_GRANTED

    init {
        channel.setMethodCallHandler(this)
        activityBinding.addRequestPermissionsResultListener(this)
        activityBinding.activity.application.registerActivityLifecycleCallbacks(
            object : Application.ActivityLifecycleCallbacks {
                override fun onActivityPaused(activity: Activity) =
                    if (cameraEnabled) barcodeView.pause() else Unit

                override fun onActivityResumed(activity: Activity) =
                    if (cameraEnabled && hasCameraPermission) barcodeView.resume() else Unit

                override fun onActivityStopped(activity: Activity) = Unit
                override fun onActivitySaveInstanceState(activity: Activity, out: Bundle) = Unit
                override fun onActivityDestroyed(activity: Activity) = Unit
                override fun onActivityCreated(activity: Activity, sis: Bundle?) = Unit
                override fun onActivityStarted(activity: Activity) = Unit
            })
    }

    override fun getView(): View {
        if (!::barcodeView.isInitialized) barcodeView = BarcodeView(context)
        return barcodeView
    }

    override fun dispose() = barcodeView.pause()

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) = when (call.method) {
        "scanStart" -> call.runMethod<List<String>>(result) { args -> scanStart(args) }
        "scanStop" -> call.runMethod<Nothing?>(result) { scanStop() }
        "setCameraEnabled" -> call.runMethod<Boolean>(result) { enabled -> setCameraEnabled(enabled) }
        "setCameraFacing" -> call.runMethod<Int>(result) { facing -> setCameraFacing(facing) }
        "enableFlash" -> call.runMethod<Boolean>(result) { enabled -> enableFlash(enabled) }
        "statusInfo" -> call.runMethod<Nothing?>(result) { statusInfo() }
        "requirePermission" -> requirePermission(result)
        else -> result.notImplemented()
    }

    private fun scanStart(arguments: List<String>): Boolean {
        if (scanEnabled) return false
        val formats = arguments.map { BarcodeFormat.valueOf(it) }
        barcodeView.decodeContinuous { result ->
            if (formats.isEmpty() || formats.contains(result.barcodeFormat)) {
                val data = mapOf(
                    "text" to result.text,
                    "rawBytes" to result.rawBytes,
                    "barcodeFormat" to result.barcodeFormat.name,
                )
                channel.invokeMethod("onBarcodeResult", data)
            }
        }
        scanEnabled = true
        return true
    }

    private fun scanStop(): Boolean {
        if (!scanEnabled) return false
        barcodeView.stopDecoding()
        scanEnabled = false
        return true
    }

    private fun setCameraEnabled(enabled: Boolean): Boolean {
        if (cameraEnabled == enabled) return false
        if (enabled) barcodeView.resume()
        else barcodeView.pause()
        cameraEnabled = enabled
        return true
    }

    private fun setCameraFacing(facing: Int): Boolean {
        val settings = barcodeView.cameraSettings
        if (settings.requestedCameraId == facing) return false
        barcodeView.pause()
        settings.requestedCameraId = facing
        barcodeView.cameraSettings = settings
        barcodeView.resume()
        return true
    }

    private fun enableFlash(enabled: Boolean): Boolean {
        if (!hasSystemFeature(FEATURE_CAMERA_FLASH) || flashEnabled == enabled) return false
        barcodeView.setTorch(enabled)
        flashEnabled = enabled
        return true
    }

    @SuppressLint("UnsupportedChromeOsCameraSystemFeature")
    private fun statusInfo(): Map<String, *> = mapOf(
        "hasCameraPermission" to hasCameraPermission,
        "availableCameras" to mutableListOf<Int>().apply {
            if (hasSystemFeature(FEATURE_CAMERA)) add(0)
            if (hasSystemFeature(FEATURE_CAMERA_FRONT)) add(1)
        },
        "hasFlash" to hasSystemFeature(FEATURE_CAMERA_FLASH),
        "cameraFacing" to barcodeView.cameraSettings.requestedCameraId,
        "cameraEnabled" to cameraEnabled,
        "flashEnabled" to flashEnabled,
        "scanEnabled" to scanEnabled,
    )

    private fun hasSystemFeature(feature: String): Boolean =
        context.packageManager.hasSystemFeature(feature)

    private lateinit var permissionResult: MethodChannel.Result
    private fun requirePermission(result: MethodChannel.Result) =
        if (hasCameraPermission) {
            result.success(true)
        } else {
            permissionResult = result
            requestPermissions(activityBinding.activity, arrayOf(CAMERA), id)
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>?,
        grantResults: IntArray
    ): Boolean {
        if (requestCode == id) {
            permissionResult.success(grantResults.all { it == PERMISSION_GRANTED })
            return true
        }
        return false
    }

    class Factory(private val messenger: BinaryMessenger) :
        PlatformViewFactory(StandardMessageCodec.INSTANCE) {

        override fun create(context: Context, id: Int, args: Any?): PlatformView =
            BarcodeView(context, id, MethodChannel(messenger, "$VIEW_TYPE/$id"))
    }

    companion object {
        const val VIEW_TYPE: String = "flutterx_barcode_view"
    }
}