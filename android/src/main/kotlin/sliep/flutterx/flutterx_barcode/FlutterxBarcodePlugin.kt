package sliep.flutterx.flutterx_barcode

import android.graphics.Bitmap
import androidx.annotation.NonNull
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import sliep.flutterx.flutterx_barcode.BarcodeView.Companion.VIEW_TYPE
import sliep.flutterx.flutterx_barcode.BarcodeView.Factory
import java.io.ByteArrayOutputStream

class FlutterxBarcodePlugin : FlutterPlugin, ActivityAware, MethodCallHandler {
    private val encoder: BarcodeEncoder = BarcodeEncoder()
    private lateinit var channel: MethodChannel

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        val messenger = flutterPluginBinding.binaryMessenger
        channel = MethodChannel(messenger, "flutterx_barcode")
        channel.setMethodCallHandler(this)
        flutterPluginBinding.platformViewRegistry.registerViewFactory(VIEW_TYPE, Factory(messenger))
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) =
        when (call.method) {
            "encodeBitmap" -> call.runMethod<Map<String, Any?>>(result) { args ->
                val contents = args["contents"] as String
                val format = BarcodeFormat.valueOf(args["format"] as String)
                val width = args["width"] as Int
                val height = args["height"] as Int
                val bitmap = encoder.encodeBitmap(contents, format, width, height)
                ByteArrayOutputStream().use { stream ->
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    bitmap.recycle()
                    stream.toByteArray()
                }
            }
            else -> result.notImplemented()
        }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) =
        channel.setMethodCallHandler(null)

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activityBinding = binding
    }

    override fun onDetachedFromActivityForConfigChanges() = Unit

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) = Unit

    override fun onDetachedFromActivity() = Unit

    companion object {
        lateinit var activityBinding: ActivityPluginBinding

        fun <T> MethodCall.runMethod(result: Result, block: (T) -> Any?) = try {
            val res = block(arguments())
            result.success(if (res is Unit) null else res)
        } catch (e: Throwable) {
            result.error("500", e.message ?: e.toString(), e)
        }
    }
}
