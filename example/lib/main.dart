import 'package:flutter/material.dart';
import 'package:flutterx_barcode/flutterx_barcode.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  CameraFacing cameraFacing = CameraFacing.back;
  bool cameraEnabled = true;
  bool flashEnabled = false;
  bool scanEnabled = true;
  Barcode? _barcode;

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Plugin example app'),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                    height: 500,
                    child: BarcodeView(
                        cameraFacing: cameraFacing,
                        cameraEnabled: cameraEnabled,
                        flashEnabled: flashEnabled,
                        scanEnabled: scanEnabled,
                        onBarcode: (barcode) => setState(() => _barcode = barcode))),
                Row(children: [
                  Switch(
                      value: cameraFacing == CameraFacing.front,
                      onChanged: (value) =>
                          setState(() => cameraFacing = value ? CameraFacing.front : CameraFacing.back)),
                  Switch(value: cameraEnabled, onChanged: (value) => setState(() => cameraEnabled = value)),
                  Switch(value: flashEnabled, onChanged: (value) => setState(() => flashEnabled = value)),
                  Switch(value: scanEnabled, onChanged: (value) => setState(() => scanEnabled = value)),
                ]),
                Text(_barcode?.text ?? ''),
                _barcode == null ? Container() : BarcodeImage.fromBarcode(barcode: _barcode!, width: 200, height: 200)
              ],
            ),
          ),
        ),
      );
}
